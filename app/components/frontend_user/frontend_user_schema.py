from app import mallow
from marshmallow import fields


Schema = mallow.Schema
String = fields.String


class FrontendUserSchema(Schema):

    id = String()

    class Meta:
        fields = ("id", "email", "verified")
