from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import BooleanField
from wtforms import PasswordField
from wtforms import ValidationError
from wtforms.validators import Email
from wtforms.validators import Length
from wtforms.validators import EqualTo
from wtforms.validators import DataRequired
from app.components.frontend_user.frontend_user_model import FrontendUser
from app.components.frontend_user.frontend_user_repository import FrontendUserRepository
from app.utilities.form.validator.exists_validator import ExistsValidator
from app.utilities.form.validator.is_unique_validator import IsUniqueValidator


class FrontendUserResetPasswordForm(FlaskForm):
    email = StringField('Email Address', [
        Email(),
        ExistsValidator(FrontendUser),
        DataRequired(),
        Length(min=6, max=32)
    ])

    class Meta:
        csrf = False


class FrontendUserSignUpForm(FlaskForm):
    email = StringField('Email Address', [
        Email(),
        DataRequired(),
        IsUniqueValidator(FrontendUser),
        Length(min=6, max=32)
    ])
    password = PasswordField('New Password', [
        DataRequired(),
        Length(min=6, max=16),
        EqualTo('password_confirm', message='Passwords must.')
    ])
    password_confirm = PasswordField('Repeat Password', [
        DataRequired(),
        Length(min=6, max=16),
        EqualTo('password', message='Passwords must match.')
    ])
    accept_tnc = BooleanField('I accept terms and conditions', [DataRequired()])

    class Meta:
        csrf = False


class FrontendUserVerificationForm(FlaskForm):
    email = StringField('Email Address', [
        Email(),
        ExistsValidator(FrontendUser),
        DataRequired(),
        Length(min=6, max=32)
    ])
    verification_code = StringField('New Password', [
        DataRequired(),
        Length(min=6, max=6)
    ])

    class Meta:
        csrf = False


class FrontendUserLoginForm(FlaskForm):
    email = StringField('Email Address', [
        Email(),
        DataRequired(),
        Length(min=6, max=32)
    ])
    password = StringField('Password', [
        DataRequired(),
        Length(min=6, max=16)
    ])

    class Meta:
        csrf = False

    def validate_email(form, field):
        user = FrontendUserRepository().find_one(email=field.data)
        if not user:
            raise ValidationError('Email is not registered in the system.')

        if not user.verified:
            raise ValidationError('Email is registered but not verified.')

    def validate_password(form, field):
        user = FrontendUserRepository().find_one(email=form.data['email'])
        if user and not user.is_valid_password(field.data):
            raise ValidationError('Invalid password.')
