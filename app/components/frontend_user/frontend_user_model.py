import json
from uuid import uuid4
from datetime import datetime
from app import mongo
from app import bcrypt
from app.base.model.address import Address
from app.base.model.metadata import Metadata
from app.constants import REGEX

Document = mongo.Document
MapField = mongo.MapField
EmailField = mongo.EmailField
StringField = mongo.StringField
BooleanField = mongo.BooleanField
DateTimeField = mongo.DateTimeField
EmbeddedDocumentField = mongo.EmbeddedDocumentField
DynamicEmbeddedDocument = mongo.DynamicEmbeddedDocument


class FrontendUserMetadata(Metadata):
    ip = StringField(regex=REGEX['ipv4'], min_length=7, max_length=15)
    address = MapField(required=False, field=EmbeddedDocumentField(Address))

    meta = {
        'strict': False,
        'allow_inheritance': False
    }


class FrontendUser(Document):
    email = EmailField(required=True, unique=True)
    password = StringField(required=True, max_length=60)
    active = BooleanField(required=True, default=False)
    anonymous = BooleanField(required=False, default=False)
    anonymous_id = StringField(regex=REGEX['uuid4'], required=False, min_length=36, max_length=36)
    tnc_accepted = BooleanField(required=True, default=False)
    verified = BooleanField(required=True, default=False)
    verification_code = StringField(required=False, default=None, max_length=6)
    metadata = EmbeddedDocumentField(FrontendUserMetadata, default=FrontendUserMetadata())
    created_at = DateTimeField(default=datetime.utcnow())
    modified_at = DateTimeField(default=datetime.utcnow())
    last_login_at = DateTimeField(required=False)

    blacklisted_fields = ['password']

    def __init__(self, *args, **kwargs):
        super(Document, self).__init__(*args, **kwargs)

    meta = {
        'strict': False,
        'collection': 'frontend_users'
    }

    # before save
    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.utcnow()
        self.modified_at = datetime.utcnow()

        return super(FrontendUser, self).save(*args, **kwargs)

    def is_valid_password(self, password_candidate):
        return bcrypt.check_password_hash(self.password, password_candidate)

    def set_password(self, password):
        self.password = self.__encrypt_password(password)

    def __encrypt_password(self, password):
        return str(bcrypt.generate_password_hash(password), 'utf-8')

    def gen_verification_code(self):
        self.verification_code = uuid4().hex[:6]

    def to_dict(self, fields='api_fields'):
        data_normalized = {}
        data = json.loads(self.to_json())
        for key in data:
            if key != '_id':
                data_normalized[key] = data[key]
            if key == '_id':
                data_normalized['id'] = data[key]['$oid']
        return data_normalized
