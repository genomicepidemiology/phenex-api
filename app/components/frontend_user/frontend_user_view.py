from uuid import uuid4
from flask import request
from flask import jsonify
from flask_classful import route
from flask_classful import FlaskView
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from app import csrf
from app.components.frontend_user.frontend_user_model import FrontendUser
from app.components.frontend_user.frontend_user_form import FrontendUserSignUpForm
from app.components.frontend_user.frontend_user_form import FrontendUserVerificationForm
from app.components.frontend_user.frontend_user_form import FrontendUserResetPasswordForm
from app.components.frontend_user.frontend_user_tasks import send_sign_up_mail
from app.components.frontend_user.frontend_user_tasks import send_reset_password_mail
from app.components.frontend_user.frontend_user_schema import FrontendUserSchema
from app.components.frontend_user.frontend_user_repository import FrontendUserRepository


class FrontendUserView(FlaskView):

    route_base = '/user'
    trailing_slash = False

    repo = FrontendUserRepository()
    schema = FrontendUserSchema()

    @jwt_required
    @route('info', methods=['GET'])
    def info(self):
        user = self.repo.find_one(id=get_jwt_identity()['id'])

        return self.schema.jsonify(user), 200

    @csrf.exempt
    @jwt_required
    @route('update', methods=['PATCH'])
    def update(self):
        user = self.repo.find_one(id=get_jwt_identity()['id'])

        if 'email' in request.json and request.json['email'] != "":
            existing_user = self.repo.find_one(email=request.json['email'])
            if existing_user:
                return jsonify({'errors': {'email': ['Provided email is already taken.']}}), 400
            user.email = request.json['email']

        if 'password' in request.json and request.json['password'] != "":
            user.set_password(request.json['password'])

        if not self.repo.save(user):
            return jsonify({'errors': 'User update failed.'}), 400

        return self.schema.jsonify(user), 200

    @csrf.exempt
    @route('sign_up', methods=['POST'])
    def sign_up(self):
        form = FrontendUserSignUpForm()
        if form.validate() is False:
            return jsonify({'errors': form.errors}), 401

        user = self.repo.create_model(email=form.data['email'])
        user.set_password(form.data['password'])
        user.gen_verification_code()
        if not self.repo.save(user):
            return jsonify({'errors': ['User sign up failed.']}), 400

        send_sign_up_mail.delay(user.email, user.verification_code)

        return jsonify({"success": True}), 201

    @csrf.exempt
    @route('verify', methods=['POST'])
    def verify(self):
        form = FrontendUserVerificationForm()
        if form.validate() is False:
            return jsonify({'errors': list(form.errors.values())[0]}), 401

        email = form.data['email']
        verification_code = form.data['verification_code']

        user = self.repo.find_one(email=email, verification_code=verification_code)
        if not user:
            return jsonify({'errors': ['Invalid verification code.']}), 401

        user.verified = True
        user.verification_code = None
        if not user.save():
            return jsonify({'errors': ['User verification failed.']}), 400

        return jsonify({"success": True}), 201

    @csrf.exempt
    @route('reset_password', methods=['POST'])
    def reset_password(self):
        form = FrontendUserResetPasswordForm()
        if form.validate() is False:
            return jsonify({'errors': form.errors}), 401

        password = uuid4().hex[:8]

        user = self.repo.find_one(email=form.data['email'])
        user.set_password(password)
        if not user.save():
            return jsonify({'errors': ['Failed to reset password.']}), 400

        send_reset_password_mail.delay(user.email, password)

        return jsonify({"success": True}), 200
