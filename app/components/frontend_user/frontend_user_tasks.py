import time
from app import celery_ext
from app.components.frontend_user.frontend_user_repository import FrontendUserRepository
from app.constants import APP_PROTOCOL
from app.constants import HOST
from app.utilities.mail import Mail
from flask import current_app

celery = celery_ext.celery


@celery.task
def send_sign_up_mail(recipient, verification_code):
    context = {
        'username': recipient.split('@')[0],
        'verification_code': verification_code,
        'verification_url': APP_PROTOCOL + '://' + HOST + '/verify_account?email=' + recipient + '&code=' + verification_code
    }

    config = {
        'subject': 'Activate your account',
        'recipients': [recipient],
        'template': 'sign_up_mail.html'
    }

    with current_app.app_context():
        Mail.send(**config, **context)


@celery.task
def send_reset_password_mail(recipient, password):
    context = {
        'username': recipient.split('@')[0],
        'password': password,
        'login_url': APP_PROTOCOL + '://' + HOST + "/"
    }

    config = {
        'subject': 'Reset password',
        'recipients': [recipient],
        'template': 'reset_password_mail.html'
    }

    with current_app.app_context():
        Mail.send(**config, **context)
