import time
from app import celery_ext
from app.constants import APP_PROTOCOL
from app.constants import HOST
from app.utilities.mail import Mail
from flask import current_app

celery = celery_ext.celery


@celery.task
def send_results_mail(message):
    if message['metadata']['output_dir']:
        attachment_dir_norm = message['metadata']['output_dir'].replace('/app/data', '/src/storage/data')

    config = {
        'subject': 'Results of the analysis',
        'recipients': [message['recipient']],
        'template': 'results_mail.html',
        'attachment_dir': attachment_dir_norm
    }

    with current_app.app_context():
        Mail.send(**config, **message)
