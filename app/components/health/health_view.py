import subprocess
from flask import jsonify
from flask_classful import route
from flask_classful import FlaskView
from app.constants import REDIS_HOST
from app.constants import MONGO_DB_HOST


class HealthView(FlaskView):

    view = 'HealthView'
    route_base = '/health'

    @route('/ready', methods=['GET'])
    def ready(self):
        # Ping Redis
        ping = subprocess.Popen(["ping", "-c", "1", REDIS_HOST],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        out, error = ping.communicate()
        if not out:
            msg = f'Redsi service is not ready yet. Error: {error}'
            return jsonify({'errors': [msg]}), 503

        # Ping Mongo
        ping = subprocess.Popen(["ping", "-c", "1", MONGO_DB_HOST],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        out, error = ping.communicate()
        if not out:
            msg = f'Mongo service is not ready yet. Error: {error}'
            return jsonify({'errors': [msg]}), 503

        # Check celery
        # TODO: make below solution working
        # link: https://stackoverflow.com/questions/8506914/detect-whether-celery-is-available-running
        ping = subprocess.Popen(["supervisorctl", "status", "app_celery"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        out, error = ping.communicate()
        if not out or 'RUNNING' not in str(out):
            msg = 'Celery service is not running'
            return jsonify({'errors': [msg]}), 503

        return jsonify({
            'status': 'ready',
            'success': True
        }), 200

    @route('/alive', methods=['GET'])
    def alive(self):
        return jsonify({
            'status': 'alive',
            'success': True
        }), 200
