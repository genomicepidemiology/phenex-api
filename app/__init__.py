import click
import jinja2
import logging
import datetime
from flask import Flask
from flask_mail import Mail
from flask_redis import Redis
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_jwt_extended import get_jwt_identity
from flask_marshmallow import Marshmallow
from flask_mongoengine import MongoEngine
from flask_wtf.csrf import CSRFProtect
from flask_celeryext import FlaskCeleryExt
from flask_cors import CORS
from flask.cli import AppGroup
from dtu_flask_tus import FlaskTusExtended
from dtu_flask_tus.models import UploadModel
from app.constants import COOKIE_DOMAIN
from app.constants import DEPLOYMENT
from app.constants import APP_HOST
from app.constants import APP_NAME
from app.constants import APP_ROOT
from app.constants import APP_DEBUG
from app.constants import APP_SECRET
from app.constants import APP_PROTOCOL
from app.constants import APP_TIMEZONE
from app.constants import APP_LOG_PATH
from app.constants import JWT_SECRET
from app.constants import JWT_ACCESS_EXPIRES
from app.constants import JWT_REFRESH_EXPIRES
from app.constants import MONGO_DB_HOST
from app.constants import MONGO_DB_PORT
from app.constants import MONGO_DB_NAME
from app.constants import MONGO_DB_USERNAME
from app.constants import MONGO_DB_PASSWORD
from app.constants import REDIS_HOST
from app.constants import REDIS_PORT
from app.constants import REDIS_DB_INDEX_JWT
from app.constants import REDIS_DB_INDEX_CELERY_BROKER
from app.constants import REDIS_DB_INDEX_CELERY_BACKEND
from app.constants import RABBITMQ_HOST
from app.constants import RABBITMQ_PORT
from app.constants import RABBITMQ_VHOST
from app.constants import RABBITMQ_USER
from app.constants import RABBITMQ_PASSWORD
from app.constants import MAIL_PORT
from app.constants import MAIL_SERVER
from app.constants import MAIL_USERNAME
from app.constants import MAIL_PASSWORD
from app.constants import MAIL_SENDER
from app.constants import TESTING_EMAIL
from app.constants import MIN_CODE_COVERAGE
from app.utilities.string import print_json


bcrypt = Bcrypt()
celery_ext = FlaskCeleryExt()
cors = CORS()
csrf = CSRFProtect()
flask_tus = FlaskTusExtended()
jwt = JWTManager()
mail = Mail()
mallow = Marshmallow()
mongo = MongoEngine()
redis = Redis()

app_queue = AppGroup('queue', help="Commands related to queuing system.")
app_test = AppGroup('test', help="Commands related to testing and debugging application.")


def create_app(config=None):
    app = Flask(__name__, static_folder=None)

    app.config['DEBUG'] = APP_DEBUG
    app.config['APP_NAME'] = APP_NAME
    app.config['APP_ROOT'] = APP_ROOT
    app.config['SECRET_KEY'] = APP_SECRET
    app.config['MIN_CODE_COVERAGE'] = MIN_CODE_COVERAGE

    host_uri = f'mongodb://{MONGO_DB_HOST}:{MONGO_DB_PORT}/{MONGO_DB_NAME}?authSource=admin'
    if MONGO_DB_PASSWORD and MONGO_DB_USERNAME:
        host_uri = f'mongodb://{MONGO_DB_USERNAME}:{MONGO_DB_PASSWORD}@{MONGO_DB_HOST}:{MONGO_DB_PORT}/{MONGO_DB_NAME}?authSource=admin'

    # NOTE: MONGODB_CONNECT or connect option fixes "MongoClient opened before fork." and Invalid databases during pytest run
    # I'm not aware of side effects yet. Default value is/was "True"
    app.config['MONGODB_SETTINGS'] = {
        'connect': False,
        'host': host_uri
    }

    app.config['REDIS_HOST'] = REDIS_HOST
    app.config['REDIS_PORT'] = REDIS_PORT
    app.config['REDIS_DB'] = REDIS_DB_INDEX_JWT
    app.config['REDIS_DECODE_RESPONSES'] = True

    app.config['RABBITMQ_HOST'] = RABBITMQ_HOST
    app.config['RABBITMQ_PORT'] = RABBITMQ_PORT
    app.config['RABBITMQ_VHOST'] = RABBITMQ_VHOST
    app.config['RABBITMQ_USER'] = RABBITMQ_USER
    app.config['RABBITMQ_PASSWORD'] = RABBITMQ_PASSWORD

    app.config['MAIL_SERVER'] = MAIL_SERVER
    app.config['MAIL_PORT'] = MAIL_PORT
    app.config['MAIL_USERNAME'] = MAIL_USERNAME
    app.config['MAIL_PASSWORD'] = MAIL_PASSWORD
    app.config['MAIL_DEFAULT_SENDER'] = MAIL_SENDER

    app.config['JWT_SECRET_KEY'] = JWT_SECRET
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = JWT_ACCESS_EXPIRES
    app.config['JWT_REFRESH_TOKEN_EXPIRES'] = JWT_REFRESH_EXPIRES
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ('access', 'refresh')
    app.config['JWT_TOKEN_LOCATION'] = ('headers', 'cookies')
    app.config['JWT_ACCESS_COOKIE_PATH'] = '/api'
    app.config['JWT_REFRESH_COOKIE_PATH'] = '/api/auth/token_refresh'
    app.config['JWT_ACCESS_CSRF_COOKIE_PATH'] = '/api'
    app.config['JWT_REFRESH_CSRF_COOKIE_PATH'] = '/api/auth/token_refresh'
    app.config['JWT_CSRF_IN_COOKIES'] = False
    app.config['JWT_COOKIE_CSRF_PROTECT'] = False
    app.config['JWT_SESSION_COOKIE'] = False
    app.config['JWT_COOKIE_DOMAIN'] = COOKIE_DOMAIN
    app.config['JWT_COOKIE_SECURE'] = not('dev' in DEPLOYMENT)

    app.config['CELERY_BROKER_URL'] = 'redis://' + app.config['REDIS_HOST'] + ':' + str(app.config['REDIS_PORT']) + '/' + str(REDIS_DB_INDEX_CELERY_BROKER)
    app.config['CELERY_BROKER_BACKEND'] = 'redis://' + app.config['REDIS_HOST'] + ':' + str(app.config['REDIS_PORT']) + '/' + str(REDIS_DB_INDEX_CELERY_BACKEND)
    app.config['CELERY_TASK_SERIALIZER'] = 'json'
    app.config['CELERY_ACCEPT_CONTENT'] = ['json']
    app.config['CELERY_RESULT_SERIALIZER'] = 'json'
    app.config['CELERY_TIMEZONE'] = APP_TIMEZONE
    app.config['CELERY_ENABLE_UTC'] = True
    app.config['CELERY_INCLUDE'] = [
        'app.components.notifications.analytics_tasks',
        'app.components.frontend_user.frontend_user_tasks',
    ]
    app.config['TUS_UPLOAD_DIR'] = '/src/storage/data/uploads/%Y/%m/%d/'
    app.config['TUS_UPLOAD_URL'] = '/api/v1/files/'
    app.config['TUS_SAMPLE_URL'] = '/api/v1/samples/'
    app.config['TUS_PRESERVE_FILE_NAME'] = False
    app.config['TUS_PRESERVE_FILE_EXTENTION'] = True
    app.config['TUS_EXPIRATION'] = datetime.timedelta(days=7)

    if config is not None:
        app.config.from_mapping(config)

    # Disable trailing slashes for all requests
    app.url_map.strict_slashes = False

    # New templates location
    app.jinja_loader = jinja2.ChoiceLoader([
        app.jinja_loader,
        jinja2.FileSystemLoader('/src/app/components/frontend_user/html'),
        jinja2.FileSystemLoader('/src/app/components/notifications/html')
    ])
    app.jinja_env.filters['format_json'] = print_json

    # Init apps
    bcrypt.init_app(app)
    celery_ext.init_app(app)
    cors.init_app(app, resources=r'/api/*')
    # csrf.init_app(app)
    flask_tus.init_app(app, model=UploadModel, db=mongo)
    jwt.init_app(app)
    mail.init_app(app)
    mallow.init_app(app)
    mongo.init_app(app)
    redis.init_app(app)

    register_cli(app)
    register_views(app)

    return app


def register_cli(app):
    from app.cli.test import send_test_mail
    from app.cli.queue import consume_message

    app.cli.add_command(app_test)
    app.cli.add_command(app_queue)


def register_views(app):
    from app.components.health.health_view import HealthView
    from app.components.frontend_user.frontend_user_view import FrontendUserView
    from app.components.auth.auth_view import AuthView

    HealthView.register(app)
    AuthView.register(app, route_prefix='/api/v1')
    FrontendUserView.register(app, route_prefix='/api/v1')


# Jwt callback decorator executed when protected endpoint is accessed
@jwt.token_in_blacklist_loader
def check_if_token_is_revoked(decrypted_token):
    token = decrypted_token['jti']
    db_token = redis.get(token)
    if db_token is None:
        return True
    return db_token == 'true'
