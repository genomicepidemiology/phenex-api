"""
Run this file with uwsgi
$ cd /srv/www/htdocs/config/services
$ uwsgi --ini app.uwsgi.development.ini --http-socket /var/run/app.sock
"""

from app import create_app
from app.constants import APP_HOST
from app.constants import APP_THREADED
from flask_celeryext import create_celery_app

app = create_app()
celery = create_celery_app(app)

# link: https://stackoverflow.com/questions/34615743/unable-to-load-configuration-from-uwsgi#answer-37175998
if __name__ == "__main__":
    app.run(host=APP_HOST, threaded=APP_THREADED)
