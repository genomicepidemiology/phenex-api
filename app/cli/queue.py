import click
import os
import pika
import shutil
import socket
import time
from app import app_queue
from app import app_test
from app import RABBITMQ_HOST
from app import RABBITMQ_PORT
from app import RABBITMQ_USER
from app import RABBITMQ_PASSWORD
from app import RABBITMQ_VHOST
from app.components.notifications.analytics_tasks import send_results_mail
from app.utilities.string import json_to_dict
from flask import current_app
from flask.cli import with_appcontext
from threading import Event
from vendors.tus.dtu_flask_tus.models.sample_models import Samples
from vendors.tus.dtu_flask_tus.models.upload_models import UploadModel

credentials = pika.PlainCredentials(RABBITMQ_USER, RABBITMQ_PASSWORD)
parameters = pika.ConnectionParameters(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_VHOST, credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()


@app_queue.command("listen", help="Listen to default queue.")
@click.argument("queue", required=False)
@with_appcontext
def consume_message(queue):
    queue = queue if queue else 'results'
    print(f'[*] Host: "{socket.gethostname()}" is listening for "{queue}" queue. To exit press CTRL+C')

    channel.queue_declare(queue, durable=True, exclusive=False)
    channel.basic_consume(queue=queue, on_message_callback=process_results)
    channel.start_consuming()

    Event().wait()


def process_results(ch, method, properties, body):
    print(f' [x] Host: "{socket.gethostname()}" received message: "{body}"')

    msg = json_to_dict(body)
    send_results_mail.delay(msg)

    ch.basic_ack(delivery_tag = method.delivery_tag)

    if msg['error']:
        handle_error(msg['metadata']['sample_id'], msg['metadata']['output_dir'])


def handle_error(sample_id, output_dir):
    """
    Delete all samples and uploads on error occurrence.
    Delay deletion in order for mail to send the attachements.
    """
    time.sleep(1)
    output_dir = output_dir.replace('/app', '/src/storage')
    sample = Samples.objects.get(id=sample_id)
    uploads = UploadModel.objects.filter(id__in=sample.uploads)
    for upload in uploads:
        if os.path.exists(upload.path):
            for sample in upload.samples:
                analysis_path = '/'.join(output_dir.split('/')[:-1]) + f'/{sample.metadata.correlation_id}'
                if os.path.exists(analysis_path):
                    shutil.rmtree(analysis_path)
            os.remove(upload.path)
        upload.delete()
    sample.delete()
