from wtforms import ValidationError


class ChoiceValidator(object):

    def __init__(self, choices, message=None):
        self.choices = choices
        if type(choices) is tuple:
            # Touple to list
            self.choices = [v for k, v in choices]

        self.message = message

    def __call__(self, form, field):
        if field.data not in self.choices:
            raise ValidationError(u"Invalid value '%s' for '%s' input." % (field.data, field.name))
