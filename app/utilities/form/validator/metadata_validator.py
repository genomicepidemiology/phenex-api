

class MetadataValidator:

    grammar = {
        'address': {
            'whitelist': ['home', 'work', 'other']
        }
    }
    form = None
    metadata = None
    message = ""

    def __init__(self, metadata, form):
        self.form = form
        self.metadata = metadata

    def validate(self):
        if None in [self.form, self.metadata]:
            return False, self.message

        metadata = self.form.get('metadata').split('.')
        metadata_name = metadata[0]
        if metadata_name not in self.grammar:
            return False, 'Metadata "%s" is not defined.' % metadata_name

        metdata_depth_0 = (len(metadata) == 1)
        metadata_depth_1 = (len(metadata) == 2)
        if metdata_depth_0:
            pass

        if metadata_depth_1:
            first_inner_key = metadata[1]
            if first_inner_key not in self.grammar[metadata_name]['whitelist']:
                return False, 'Metadata key "%s" is not allowed.' % first_inner_key

        return True, self.message
