import re
from wtforms import ValidationError


class CustomRegexValidator(object):

    def __init__(self, regex=None, allow_empty=False, message=None):
        self.regex = regex
        self.allow_empty = allow_empty
        self.message = message

    def __call__(self, form, field):
        if self.allow_empty is False and field.data == '':
            raise ValidationError(u"Invalid empty value for '%s' input." % (field.name))

        if self.allow_empty and field.data != '':
            match = re.match(self.regex, field.data)
            if match is None:
                raise ValidationError(u"Value '%s' does not match regex '%s' input." % (field.data, self.regex))
