import os
from app import mail
from flask import current_app
from flask import render_template
from flask_mail import Message


class Mail:

    @staticmethod
    def send(subject, recipients, template=None, body='', attachment_dir='', **context):
        message = Message(subject=subject, recipients=recipients)
        message.body = body

        # Bind HTML jinja template with context data
        if template:
            message.html = render_template(template, **context)

        # Attach files from attachment directory
        try:
            if attachment_dir:
                files = os.listdir(attachment_dir)
                for file in files:
                    file_path = f'{attachment_dir}/{file}'
                    if not os.path.isdir(file_path):
                        with current_app.open_resource(file_path) as fp:
                            message.attach(file, "text/plain", fp.read())
        except OSError:
            pass

        # TODO figureout what to do with email when failed to send them
        try:
            mail.send(message)
            return True
        except Exception as e:
            return False
