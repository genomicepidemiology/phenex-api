from app.constants import PAGINATION_PAGE_SIZE


class Repository:

    __model = None

    def __init__(self, model):
        self.__model = model

    def model(self, *args, **kwargs):
        return self.__model

    def create_model(self, *args, **kwargs):
        if args or kwargs:
            return self.__model(*args, **kwargs)
        return self.__model()

    def save(self, model):
        try:
            return model.save()
        except Exception:
            return None

    def update(self,  model, **kwargs):
        try:
            if kwargs:
                return model.update(**kwargs)
            return model.save()
        except Exception:
            return None

    def find_one(self, **kwargs):
        try:
            return self.__model.objects(**kwargs).get()
        except Exception:
            return None

    def filter(self, **kwargs):
        try:
            return self.__model.objects().filter(**kwargs)
        except Exception:
            return None

    def find_all(self, **kwargs):
        try:
            return self.__model.objects(**kwargs).all()
        except Exception:
            return None

    def delete_all(self):
        return self.__model.objects.delete()

    def serialize(self, **kwargs):
        pass

    def paginate(self, page=1,  per_page=PAGINATION_PAGE_SIZE, sort_by=None):
        try:
            if sort_by is None:
                return self.__model.objects().paginate(page=page, per_page=per_page)
            else:
                return self.__model.objects().order_by(sort_by).paginate(page=page, per_page=per_page)
        except Exception:
            return None
