#!/bin/ash

set -e

# Check if server runs uwsgi server
server=$(curl -sI localhost/api/v1/demo | grep "Server: ")
status_uwsgi=$(echo $server | grep "uwsgi")
if [ -z "$status_uwsgi" ]; then
  echo 'WSGI server uwsgi is not running'
  exit 1
fi

coverage run -m pytest tests/

# Check if test coverage is within acceptable % socre
COVERAGE_SCORE=$(coverage report | grep TOTAL | awk '{print $4}' | sed 's/\%//g')
if [ "$COVERAGE_SCORE" -lt "$MIN_CODE_COVERAGE" ]; then
  echo "Coverage Score of $COVERAGE_SCORE% is not acceptable!"
  echo "Required value cannot be smaller than $MIN_CODE_COVERAGE%"
  exit 1
fi
