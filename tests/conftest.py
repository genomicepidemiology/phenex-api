import pytest
from app import mongo
from app import create_app
from app.constants import REDIS_HOST
from app.constants import REDIS_PORT
from app.constants import TESTING_EMAIL
from app.constants import TESTING_PASSWORD
from app.constants import TESTING_MONGO_DB_NAME
from app.constants import TESTING_REDIS_DB_INDEX_JWT
from app.constants import TESTING_REDIS_DB_INDEX_CELERY_BROKER
from app.constants import TESTING_REDIS_DB_INDEX_CELERY_BACKEND
from app.components.frontend_user.frontend_user_repository import FrontendUserRepository
from .helpers import AuthActions


@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
        'TESTING_EMAIL': TESTING_EMAIL,
        'TESTING_PASSWORD': TESTING_PASSWORD,
        'MONGODB_DB': TESTING_MONGO_DB_NAME,
        'REDIS_DB': TESTING_REDIS_DB_INDEX_JWT,
        'CELERY_BROKER_URL': 'redis://' + REDIS_HOST + ':' + str(REDIS_PORT) + '/' + str(TESTING_REDIS_DB_INDEX_CELERY_BROKER),
        'CELERY_BROKER_BACKEND': 'redis://' + REDIS_HOST + ':' + str(REDIS_PORT) + '/' + str(TESTING_REDIS_DB_INDEX_CELERY_BACKEND),
    })

    with app.app_context():
        delete_database(app)

    return app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def repo():
    return {'frontend_user': FrontendUserRepository()}


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


@pytest.fixture
def auth(app, client, repo):
    repo = repo['frontend_user']
    email = app.config['TESTING_EMAIL']
    password = app.config['TESTING_PASSWORD']

    return AuthActions(client, repo, email, password)


def delete_database(app):
    mongo.connection.drop_database(app.config['MONGODB_DB'])
