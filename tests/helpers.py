

class AuthActions(object):

    def __init__(self, client, repo, email, password):
        self.email = email
        self.password = password
        self._repo = repo
        self._client = client

    def register(self, email=None, password=None):

        data = {
            'email': email if email else self.email,
            'password': password if password else self.password,
            'password_confirm': password if password else self.password,
            'accept_tnc': True
        }

        return self._client.post('/api/v1/user/sign_up', data=data)

    def verify(self):
        user = self._repo.find_one(email=self.email)
        data = {
            'email': self.email,
            'verification_code': user.verification_code
        }
        return self._client.post('/api/v1/user/verify', data=data)

    def login(self, email=None, password=None):

        data = {
            'email': email if email else self.email,
            'password': password if password else self.password
        }

        return self._client.post('/api/v1/auth/login', data=data)

    def refresh_token(self, refresh_token=''):
        headers = {'Authorization': 'Bearer ' + refresh_token}
        return self._client.post('/api/v1/auth/token_refresh', headers=headers)

    def logout(self, access_token=''):
        headers = {'Authorization': 'Bearer ' + access_token}
        return self._client.delete('/api/v1/auth/logout', headers=headers)
