import pytest
import subprocess

from app import mongo
from app.constants import REDIS_HOST
from app.constants import RABBITMQ_HOST
from app.constants import MONGO_DB_HOST
from app.constants import TESTING_MONGO_DB_NAME
from pythonping import ping


def test_config(app):
    assert app.config['MONGODB_DB'] == TESTING_MONGO_DB_NAME
    # check that app database is connected with correct client
    # assert mongo.get_db().name == app.config['MONGODB_DB']
    # assert app.testing is True


def test_redis_service():
    try:
        status = ping(REDIS_HOST, count=1)
    except Exception:
        status = 'not responding'

    assert status != 'not responding'


def test_rabbitmq_service():
    try:
        status = ping(RABBITMQ_HOST, count=1)
    except Exception:
        status = 'not responding'

    assert status != 'not responding'


def test_mongo_service():
    try:
        status = ping(MONGO_DB_HOST, count=1)
    except Exception:
        status = 'not responding'

    assert status != 'not responding'


def test_celery_service():
    output = subprocess.check_output(["supervisorctl", "status", "app_celery"])

    assert 'RUNNING' in str(output)
