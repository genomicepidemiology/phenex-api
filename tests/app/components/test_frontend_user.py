import pytest


verification_endpoint = '/api/v1/user/verify'
reset_password_endpoint = '/api/v1/user/reset_password'


def test_sign_up_endpoint(auth, repo):
    repo['frontend_user'].delete_all()

    response = auth.register()
    assert response.status_code == 201

    response = auth.register()
    assert response.status_code == 401
    assert 'email already exist' in response.get_json()['errors']['email'][0]


def test_verify_endpoint(app, auth, client, repo):
    response = auth.register()
    user = repo['frontend_user'].find_one(email=app.config['TESTING_EMAIL'])

    fake_verification_code = '123456'
    long_verification_code = 'fake' + user.verification_code

    response = client.post(verification_endpoint, data={'email': user.email})

    assert response.status_code == 401
    assert 'field is required' in response.get_json()['errors'][0]

    response = client.post(verification_endpoint, data={
        'email': 'fake@mail.com',
        'verification_code': long_verification_code
    })

    assert response.status_code == 401
    assert 'not exist' in response.get_json()['errors'][0]

    response = client.post(verification_endpoint, data={
        'email': user.email,
        'verification_code': long_verification_code
    })
    assert response.status_code == 401
    assert 'Field must be exactly 6 characters long' in response.get_json()['errors'][0]

    response = client.post(verification_endpoint, data={
        'email': user.email,
        'verification_code': fake_verification_code
    })
    assert response.status_code == 401
    assert 'Invalid verification code' in response.get_json()['errors'][0]

    response = client.post(verification_endpoint, data={
        'email': user.email,
        'verification_code': user.verification_code
    })
    assert response.status_code == 201


def test_reset_password_endpoint(client, auth, repo):
    repo['frontend_user'].delete_all()

    response = auth.register()
    assert response.status_code == 201

    response = client.post(reset_password_endpoint, data={'email': 'fake@mail.com'})
    assert response.status_code == 401
    assert 'email does not exist' in response.get_json()['errors']['email'][0]

    response = client.post(reset_password_endpoint, data={'email': auth.email})
    assert response.status_code == 200
