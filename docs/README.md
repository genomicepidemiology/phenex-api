# Dockerfile

```bash
# Development
$ docker build  --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEBUG=true \
  -t genomicepidemiology/phenex-backend:dev \
  -f docker/Dockerfile.development .

# Testing
$ docker build  --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEBUG=true \
  -t genomicepidemiology/phenex-backend:test \
  -f docker/Dockerfile.testing .

# Production
$ docker build  --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEBUG=true \
  -t genomicepidemiology/phenex-backend:prod \
  -f docker/Dockerfile.production .

$ docker run --name phenex_dev_api --rm -d genomicepidemiology/phenex-api:dev
$ docker run --name phenex_test_api --rm -d genomicepidemiology/phenex-api:test
$ docker run --name phenex_api --rm -d genomicepidemiology/phenex-api:prod
```


# Docker Compose

```bash
# Build images automatically
$ docker-compose -f composer-development.yml build
$ docker-compose -f composer-testing.yml build
$ docker-compose -f composer-production.yml build

# Run project in foreground
$ docker-compose -f composer-development.yml up
$ docker-compose -f composer-testing.yml up
$ docker-compose -f composer-production.yml up

# Run project in background
$ docker-compose -f composer-development.yml up -d
$ docker-compose -f composer-testing.yml up -d
$ docker-compose -f composer-production.yml up -d
```

# Testing

```bash
# Run tests.sh
$ . tests/tests.sh

# Pytest
$ pytest -v tests/

# Check test coverage
$ coverage run -m pytest
$ coverage report
```

# Backing services

## Mailgun

[docs](https://documentation.mailgun.com/en/latest/quickstart-sending.html#send-via-smtp)

```bash
$ curl -s --user 'api:<API Key>' \
    https://api.mailgun.net/v3/<DOMAIN>.mailgun.org/messages \
    -F from='noreply@food.dtu.com' \
    -F to=ludd@food.dtu.dk \
    -F subject='Hello' \
    -F text='Testing some Mailgun awesomeness!'

$ ./swaks --auth \
      --server smtp.mailgun.org:587 \
      --au <Default SMTP Login> \
      --ap <Default Password> \
      --from noreply@food.dtu.dk \
      --to ludd@food.dtu.dk \
      --h-Subject: "Hello" \
      --body 'Testing some Mailgun awesomness!'
```
